library stripe_services;

import 'dart:developer';

import 'package:dio/dio.dart';
import 'package:flutter/foundation.dart';

import 'package:flutter_stripe/flutter_stripe.dart';
import 'package:logger/logger.dart';
import 'package:stripe_services/src/network/dio_client.dart';
import 'package:stripe_services/src/singleton.dart';
import 'package:stripe_services/stripe_services.dart';

export 'src/models/payment_Intent.dart';



part 'src/stripe_helper.dart';

typedef PaymentIntentCallback = dynamic Function(List<PaymentIntents> cards);



BaseOptions options = BaseOptions(baseUrl: Singleton().getBaseUrl());
Dio dio = Dio(options);


class SFLStripeServices {

  final StripeHelper _stripeHelper;

  SFLStripeServices._(this._stripeHelper);

  factory SFLStripeServices({
    required String serverUrl,
    required String publicKey}) {

    final stripeHelper = StripeHelper();

    stripeHelper.configure(serverUrl: serverUrl, publicKey: publicKey);

    return SFLStripeServices._(stripeHelper);
  }

  void getPaymentIntent(String customerId) {
    _stripeHelper.getPaymentIntent(customerId: customerId);
  }

  void onPaymentIntent(PaymentIntentCallback paymentIntentCallback) {
    _stripeHelper.paymentIntentCallback = paymentIntentCallback;
  }

}

