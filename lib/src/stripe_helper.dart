part of stripe_services;

class StripeParameter {
  static const customerId = 'cust_id';
}

//final logger = Logger('SFLStripeHelperServices');

class StripeHelper {
  StripeHelper._();

  static final StripeHelper _instance = StripeHelper._();

  factory StripeHelper() => _instance;

  final DioClient dioClient = DioClient();

  PaymentIntentCallback? paymentIntentCallback;


  void configure({
    required String serverUrl,
    required String publicKey}) {

    Stripe.publishableKey = publicKey;
    Singleton().setBaseUrl(serverUrl);

    // logger.info('::::: Stripe Configure :::::');

  }

  getPaymentIntent({required String customerId}) async {
    var paymentIntent = await dioClient.getPaymentIntents(customerId: customerId);
    log("paymentIntent --> $paymentIntent");
    paymentIntentCallback!(paymentIntent);
  }

  // static void enableLogger() {
  //   Logger.root.level = Level.INFO; // defaults to Level.INFO
  //   Logger.root.onRecord.listen((record) {
  //     if (kDebugMode) {
  //       log('${record.level.name}: ${record.time}: ${record.message}');
  //     }
  //   });
  // }

}