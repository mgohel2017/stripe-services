import 'dart:convert';
import 'dart:developer';

import 'package:dio/dio.dart';
import 'package:flutter/foundation.dart' show kDebugMode;
import 'package:stripe_services/src/models/models.dart';
import 'dio_exception.dart';
import 'interceptors/interceptors.dart';

import 'endpoints.dart';



class DioClient {
  DioClient()
      : _dio = Dio(
          BaseOptions(
            baseUrl: Endpoints.baseURL,
            connectTimeout: Endpoints.connectionTimeout,
            receiveTimeout: Endpoints.receiveTimeout,
            responseType: ResponseType.json,
          ),
        )..interceptors.addAll([
            LoggerInterceptor(),
          ]);

  late final Dio _dio;

  Future<List<PaymentIntents>> getPaymentIntents({required String customerId}) async {
    try {
      final response = await _dio.get(Endpoints().getAllPaymentMethodsByCustomerId(customerId));
      List<PaymentIntents> result =[];
      response.data.forEach((element) {
        result.add(PaymentIntents.fromMap(element));
      });
      return result;
    } on DioError catch (err) {
      final errorMessage = DioException.fromDioError(err).toString();
      throw errorMessage;
    } catch (e) {
      if (kDebugMode) print(e);
      throw e.toString();
    }
  }
}
