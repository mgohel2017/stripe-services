import '../singleton.dart';

class Endpoints {

  static String baseURL = Singleton().getBaseUrl();

  static const int receiveTimeout = 5000;

  static const int connectionTimeout = 3000;

  static const String users = '/users';

  String getAllPaymentMethodsByCustomerId(String customerId) {
    return baseURL + "api/customers/${customerId}/payment-method";
  }

}
