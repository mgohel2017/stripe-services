//
//
// import 'dart:collection';
// import 'dart:convert';
// import 'dart:developer';
//
// import 'package:dio/dio.dart';
// import 'package:flutter/cupertino.dart';
//
// import '../../stripe_services.dart';
// import '../endpoints.dart';
//
// class Repository {
//   Future<List<PaymentIntents>> getPaymentIntent(String customerId) async {
//     List<PaymentIntents> paymentIntent = [];
//     log("Url --> ${Endpoints().getAllPaymentMethodsByCustomerId(customerId)}");
//     log("customerId --> ${customerId}");
//     try {
//       Response response = await dio.get(Endpoints().getAllPaymentMethodsByCustomerId(customerId));
//       log("response --> ${response.data}");
//
//       if (response.statusCode == 200) {
//
//         Iterable l = json.decode(response.data);
//         List<PaymentIntents> posts = List<PaymentIntents>.from(l.map((model)=> PaymentIntents.fromJson(model)));
//         // Map<String, dynamic> result = HashMap<String, dynamic>();
//         // List<PaymentIntents> d = [PaymentIntents].fromJson(response.data);
//         // List<dynamic> d = result["content"];
//         posts.forEach((element) {
//           paymentIntent.add(element);
//         });
//       }
//       return paymentIntent;
//     } catch (exception) {
//       debugPrint("exception--->$exception");
//       return paymentIntent;
//     }
//   }
// }
