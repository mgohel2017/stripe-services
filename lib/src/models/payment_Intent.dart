
import 'dart:convert';

import 'package:equatable/equatable.dart';
import 'package:json_annotation/json_annotation.dart';

part 'payment_Intent.g.dart';

@JsonSerializable()
class PaymentIntents extends Equatable {
  PaymentIntents({
    this.stripePaymentMethodId,
    this.stripeCustomerId,
    this.cardName,
    this.expMonth,
    this.expYear,
    this.last4,
    this.cardBrand,
    this.countryCode,
    //this.isDefault
  });

  String? stripePaymentMethodId;
  String? stripeCustomerId;
  String? cardName;
  String? expMonth;
  String? expYear;
  String? last4;
  String? cardBrand;
  String? countryCode;
  //bool? isDefault;

  factory PaymentIntents.fromJson(String str) => PaymentIntents.fromMap(json.decode(str));

  String toJson() => json.encode(toMap());

  factory PaymentIntents.fromMap(Map<String, dynamic> json) => PaymentIntents(
    stripePaymentMethodId: json["stripePaymentMethodId"],
    stripeCustomerId: json["stripeCustomerId"],
    cardName: json["cardName"],
    expMonth: json["expMonth"],
    expYear: json["expYear"],
    last4: json["last4"],
    cardBrand: json["cardBrand"],
    countryCode: json["countryCode"],
   // isDefault: json["isDefault"]
  );

  Map<String, dynamic> toMap() => {
    "stripePaymentMethodId" : stripePaymentMethodId,
    "stripeCustomerId" : stripeCustomerId,
    "cardName" : cardName,
    "expMonth" : expMonth,
    "expYear" : expYear,
    "last4" : last4,
    "cardBrand" : cardBrand,
    "countryCode" : countryCode,
    //"isDefault" : isDefault
  };

  @override
  List<Object?> get props => [stripePaymentMethodId];
}
