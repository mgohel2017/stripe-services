// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'payment_Intent.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

PaymentIntents _$PaymentIntentsFromJson(Map<String, dynamic> json) =>
    PaymentIntents(
      stripePaymentMethodId: json['stripePaymentMethodId'] as String?,
      stripeCustomerId: json['stripeCustomerId'] as String?,
      cardName: json['cardName'] as String?,
      expMonth: json['expMonth'] as String?,
      expYear: json['expYear'] as String?,
      last4: json['last4'] as String?,
      cardBrand: json['cardBrand'] as String?,
      countryCode: json['countryCode'] as String?,
    );

Map<String, dynamic> _$PaymentIntentsToJson(PaymentIntents instance) =>
    <String, dynamic>{
      'stripePaymentMethodId': instance.stripePaymentMethodId,
      'stripeCustomerId': instance.stripeCustomerId,
      'cardName': instance.cardName,
      'expMonth': instance.expMonth,
      'expYear': instance.expYear,
      'last4': instance.last4,
      'cardBrand': instance.cardBrand,
      'countryCode': instance.countryCode,
    };
