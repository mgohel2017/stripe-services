
class Singleton {
  Singleton._();
  static final Singleton _instance = Singleton._();
  factory Singleton() => _instance;

  late String baseUrl = "http://localhost:8080/";

  setBaseUrl(String url) {
    baseUrl = url;
  }

  String getBaseUrl() {
    return baseUrl;
  }

}
