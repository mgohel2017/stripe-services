
import 'package:flutter/material.dart';
import 'package:sfl_flutter_package/sfl_flutter_package.dart';
import 'package:stripe_services/stripe_services.dart';

import 'credit_card_cell.dart';

late final SFLStripeServices stripeServices;

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  // set the publishable key for Stripe - this is mandatory

  String remoteServer = "http://localhost:8080/";
  String publishableKey = 'pk_test_51K96poPG3Nvf8hZ6XeUr4TiQGRU4kFe1NzKs2cUA8wQrqvigAdfTCNpUBWei7iZH2lR1udfqMhikHgPIvmOe9BOB00OmIFycIU';
  stripeServices = SFLStripeServices(serverUrl: remoteServer,publicKey: publishableKey);
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Strip Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(title: 'Strip Demo'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key? key, required this.title}) : super(key: key);

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  // CardFieldInputDetails? _card;
  // var _editController = CardEditController();

  late List<PaymentIntents?> _creditCardList = [];


  @override
  void initState() {
    super.initState();

    stripeServices.getPaymentIntent("cus_Lt4FxYERqbzKQA");

    stripeServices.onPaymentIntent((paymentIntent) {
      setState(() {
        _creditCardList = paymentIntent;
      });
    });
    
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Column(
        children: <Widget>[
          Expanded(
            child: Container(
                padding: EdgeInsets.only(top: 20), child: _cardListing()),
          ),
        ],
      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }

  // void _addCard() {
  //   _editController = CardEditController();
  //   _card = null;
  //
  //   showDialog<void>(
  //     context: context,
  //     barrierDismissible: false,
  //     // false = user must tap button, true = tap outside dialog
  //     builder: (BuildContext dialogContext) {
  //       return Dialog(
  //         elevation: 10,
  //         child: Padding(
  //           padding: const EdgeInsets.fromLTRB(8, 14, 8, 14),
  //           child: Container(
  //             height: 160,
  //             child: Column(
  //               children: [
  //                 Text('Credit Card', style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),),
  //                 SizedBox(height: 20,),
  //                 CardField(
  //                   controller: _editController,
  //                   onCardChanged: (card) {
  //                     setState(() {
  //                       _card = card;
  //                     });
  //                   },
  //                 ),
  //                 SizedBox(height: 12,),
  //                 Row(
  //                   mainAxisAlignment: MainAxisAlignment.spaceEvenly,
  //                   children: [
  //                     TextButton(
  //                       onPressed: _handleSavePress,
  //                       child: Text('Save', style: getButtonStyle(),),
  //                     ),
  //                     TextButton(
  //                       onPressed: () {
  //                         Navigator.of(context).pop();
  //                       },
  //                       child: Text('Cancel', style: getButtonStyle(),),
  //                     ),
  //                   ],
  //                 )
  //               ],
  //             ),
  //           ),
  //         ),
  //       );
  //     },
  //   );
  // }
  //
  // void _handleSavePress() async {
  //   if (_card == null || _card?.complete == false) {
  //     SFLToast.display(context: context, message:'Please enter valid card details');
  //     return;
  //   }
  //   FocusScope.of(context).unfocus();
  //   try {
  //     final billingDetails = BillingDetails(
  //       email: 'email@stripe.com',
  //       phone: '+48888000888',
  //       address: Address(
  //         city: 'Houston',
  //         country: 'US',
  //         line1: '1459  Circle Drive',
  //         line2: '',
  //         state: 'Texas',
  //         postalCode: '77063',
  //       ),
  //     ); //
  //     final paymentMethod =
  //         await Stripe.instance.createPaymentMethod(PaymentMethodParams.card(
  //           setupFutureUsage: PaymentIntentsFutureUsage.OnSession,
  //           billingDetails: billingDetails,
  //     ));
  //     print(paymentMethod.id);
  //     Navigator.of(context).pop();
  //   }catch (e) {
  //     print(e.toString());
  //   }
  // }
  //

  Widget _cardListing() => _creditCardList.isNotEmpty
      ? Column(
    children: <Widget>[
      Expanded(
        child: ListView.builder(
          itemCount: _creditCardList.length,
          itemBuilder: (context, index) => index >= _creditCardList.length
              ? Container(
            color: Colors.transparent,
            child: SFLLoader(),
          )
              : CreditCardCell(
              _creditCardList[index] ?? PaymentIntents(), index, _creditCardList.length, () {}),
        ),
      ),
    ],
  )
      : _placeholderView();

  Widget _placeholderView() => Container(
    //padding: EdgeInsets.symmetric(horizontal: 20),
    margin: EdgeInsets.symmetric(horizontal: 50),
    child: Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text(
            'CardNotAdded',
          ),
          SizedBox(
            height: MediaQuery.of(context).size.height * 0.03,
          ),
          Text(
            'AddCard',
            textAlign: TextAlign.center,
          ),
        ],
      ),
    ),
  );




}

TextStyle getButtonStyle() {
  return TextStyle(fontSize: 16,);
}





