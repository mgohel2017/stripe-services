
import 'package:flutter/material.dart';
import 'package:stripe_services/stripe_services.dart';

class CreditCardCell extends StatelessWidget {
  late final PaymentIntents creditCard;

  void Function() onDeletePressed;

  int index;
  int length;

  CreditCardCell(this.creditCard, this.index,this.length, this.onDeletePressed);

  @override
  Widget build(BuildContext context) => Column(
    children: <Widget>[
      Container(
        decoration: BoxDecoration(
          color: Colors.white,
          //borderRadius: BorderRadius.circular(10),
          boxShadow: [
            BoxShadow(
                color: Colors.grey,
                //spreadRadius: 1.0,
                blurRadius: 12.0,
                offset: Offset(2.0, 5.0))
          ],
        ),
        child: Column(
          children: [
            Container(
              padding: EdgeInsets.all(20),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Container(

                    //margin: const EdgeInsets.all(20),
                    //EdgeInsets.fromLTRB(15.0, 15.0, 0.0, 15.0),
                    child: Container(
                      color: Colors.transparent,
                      width: 50.0,
                      height: 50.0,
                    ),
                    decoration: BoxDecoration(
                      color: Colors.transparent,
                    ),
                  ),
                  SizedBox(
                    width: 20,
                  ),
                  Text(
                    '**** **** ',
                  ),
                  Text(
                    creditCard.last4 ?? "",
                  ),
                  TextButton(
                    onPressed: onDeletePressed,
                    child: Text(
                      "DELETE",
                    ),
                  ),
                  Expanded(
                      child: Text(
                        'Verified', //'Default'
                        textAlign: TextAlign.right,
                      ),),
                ],
              ),
            ),
            Container(
              margin: EdgeInsets.symmetric(horizontal: 20),
              height: 1,
              color: Colors.grey,
            ),
            Container(
              padding: EdgeInsets.symmetric(vertical: 15,horizontal: 20),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    creditCard.cardName ?? "",
                  ),
                  Text(
                    (creditCard.expMonth ?? "") +'/' + (creditCard.expYear ?? ""),
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    ],
  );
}
